import {classNames} from "_style";
import {ReactNode} from "react";

interface ISelectOption {
    label: string;
    value: string;
    isMarked?: boolean;
}

export interface ISelectComponent {
    customClass?: string;
    className?: classNames;
    options?: ISelectOption[];
    creatable?: boolean;
    children?: ReactNode;

    [id: string]: any;
}
