import {classNames} from "_style";

export interface ILogoInterface {
    className?: classNames;
    logoName?: string;
}
