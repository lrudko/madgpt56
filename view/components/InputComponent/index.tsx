import {PureComponent} from "_component/PureComponent";
import makeId from "_utils/makeid";
import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";
import InputMask from "react-input-mask";
import AutosizeTextarea from "react-textarea-autosize";
import {IInputComponent} from "./interface";

const AutosizeInput = require("react-input-autosize").default;
const InputNumber = require("react-number-format");

export class InputComponent extends React.PureComponent<IInputComponent, {}> {

    public static defaultProps: IInputComponent = {
        className: getStyle("input-block", "input"),
        type: "input",
        addon: null,
    };

    private IdLabel: string;
    private IdInput: string;
    private IdError: string;

    public componentWillMount() {
        const idTemp = makeId();
        this.IdLabel = "label" + idTemp;
        this.IdInput = "input" + idTemp;
        this.IdError = "error" + idTemp;
    }

    public render() {
        const children: any[] = [];

        const {className, classNameLayout, disabled, error, type, autosize, label, mask, addon, ...otherProps} = this.props;
        const classes = initStyle(getStyle("input-block", type === "number" ? "input" : type), className);
        const classesLayout = [{
            [getStyle("input-block")]: true,
            [getStyle("input-block", null, "error")]: !!error,
            [getStyle("input-block", null, "disabled")]: !!disabled,
        }, classNameLayout];

        if (typeof label === "string") {
            children.push(<label key={this.IdLabel} htmlFor={this.IdInput} className={getStyle("input-block", "label")}>{label}</label>);
        }

        children.push((() => {
            switch (false) {
                case !(type === "number"):
                    return <InputNumber key={this.IdInput} id={this.IdInput} className={classes} {...otherProps} />;
                case !(typeof mask === "string" && process.env.BROWSER):
                    return <InputMask key={this.IdInput} id={this.IdInput} className={classes} mask={mask} {...otherProps} />;
                case !(type === "input" && autosize === true):
                    return <AutosizeInput key={this.IdInput} id={this.IdInput} inputClassName={classes} disabled={disabled} {...otherProps} />;
                case !(type === "textarea" && process.env.BROWSER && autosize === true):
                    return <AutosizeTextarea key={this.IdInput} id={this.IdInput} className={classes} disabled={disabled} {...otherProps} />;
                case !(type === "textarea" && (!process.env.BROWSER || autosize !== true)):
                    return <textarea key={this.IdInput} id={this.IdInput} className={classes} disabled={disabled} {...otherProps} />;
                default:
                    return <input key={this.IdInput} id={this.IdInput} className={classes} disabled={disabled} type={type} {...otherProps} />;
            }
        })());
        if (addon){
             children.push(<label key={this.IdInput + "_title"} >{addon}</label>);
        }
        if (typeof error === "string") {
            children.push(<div key={this.IdError} className={getStyle("input-block", null, "error")}>{error}</div>);
        }

        return (
           <PureComponent tag={"span"} className={classesLayout} children={children} />
        );
    }
}

export default InputComponent;
