import noob from "_utils/noob";
import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";
import {ITabBlock, ITabComponent, ITabComponentState, ITabHeader} from "./interface";

export class Tab extends React.Component<ITabComponent, ITabComponentState> {

    public static defaultProps: ITabComponent = {
        className: getStyle("tab"),
        selected: 0,
    };

    constructor(props: any) {
        super(props);
        this.changeActive = this.changeActive.bind(this);
        this.state = {
            idTab: null,
        };
    }

    public changeActive(idTab: string | number): void {
        this.setState({idTab});
    }

    public render() {

        const {className, selected, children = [], ...otherProps} = this.props;
        const classes = initStyle(getStyle("tab"), className);
        const {idTab} = this.state;

        const activeTab = (idTab || selected);
        const tabsHeader: React.ReactNode[] = (Array.isArray(children) && children || [children]).filter((element: React.ReactNode) => !!((element as any).props && (element as any).props.forId));
        const tabsBlock: React.ReactNode[] = (Array.isArray(children) && children || [children]).filter((element: React.ReactNode) => !!((element as any).props && (element as any).props.idTab));

        return (
            <div className={classes} {...otherProps}>
                <ul className={getStyle("tab", "header")}>
                    {tabsHeader.map(({props}: any, key) => <TabHeader key={key} {...props} isActive={activeTab === props.forId} changeActive={this.changeActive}/>)}
                </ul>
                <div className={getStyle("tab", "block")}>
                    {tabsBlock.map(({props}: any, key) => <TabBlock key={key} {...props} isActive={activeTab === props.idTab}/>)}
                </div>
            </div>
        );
    }
}

export const TabHeader: React.SFC<ITabHeader> = (props: ITabHeader) => {
    const {className, children, isActive = false, forId, changeActive, onClick = noob, display, ...otherProps} = props;
    const headerClasses = initStyle({
        [getStyle("tab", "link")]: true,
        [getStyle("tab", "link", "selected")]: isActive,
    });

    const changeTab = (event: MouseEvent): void => { onClick(event); changeActive(forId); };
    return (<li className={initStyle(headerClasses, className)} style={{display}} children={children} onClick={changeTab as any} {...otherProps} />);
};

export const TabBlock: React.SFC<ITabBlock> = (props: ITabBlock) => {
    const {className, children, idTab, isActive, ...otherProps} = props;
    const blockClasses = initStyle({
        [getStyle("tab", "panel")]: true,
        [getStyle("tab", "panel", "selected")]: isActive,
    });
    return (<div className={initStyle(blockClasses, className)} children={children} {...otherProps} />);
};
