import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import {RouterStore} from "mobx-react-router";
import * as React from "react";

@inject("routing")
@observer
export class MenuComponent extends React.Component<{routing?: RouterStore }, {}> {

    public render() {
        const {routing} = this.props;
        const navigationMenuStyle = routing.location.pathname === "/trading" ? "-on-trading" : "";
        const navigationStyle = "-on-trading";
        return (
            <div className={initStyle(getStyle("navigation" + navigationStyle))}>
                <nav className={initStyle(getStyle("menu", "nav" + navigationMenuStyle), getStyle("nav-menu"))}>
                    {}
                </nav>
            </div>
        );
    }
}

export default MenuComponent;
