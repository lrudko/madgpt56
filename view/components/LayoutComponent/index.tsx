import HeaderComponent from "_component/HeaderComponent";
import {getStyle, initStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import {RouterStore} from "mobx-react-router";
import * as React from "react";
import Scrollbars from "react-custom-scrollbars";

// const Arbitrage = LazyLoadComponent(() => System.import("_page/Arbitrage"), LoadingComponent, ErrorComponent);

const scrollBarStyle = {
    position: "absolute" as "absolute",
    top: "80px",
    width: "calc(100% - 40px)",
    height: "calc(100% - 96px)",
    left: "20px",
    transition: "all .5s ease-in-out",
};

const scrollBarStyleFullWidth = {
    ...scrollBarStyle, ...{
        width: "100%",
        left: "0",
        transition: "none",
        height: "calc(100% - 72px)",
        top: "72px",
    }
};

@inject("routing")
@observer
export class LayoutComponent extends React.Component<{ routing?: RouterStore }, {}> {

    constructor(props) {
        super(props);
    }

    public hideOverlaysOnScroll() {
        const children = document.body.children;
        for (let j = 0; j < children.length; j++) {
            if (children[j].id.indexOf("compactViewBlock") > -1) {
                (children[j] as any).style.display = "none";
            }
        }
    }

    public componentDidMount() {
    }

    public render() {
        const {routing} = this.props;
        let containerStyles;
        const isPortfolio = routing.location.pathname === "/portfolio";
        if (routing.location.pathname === "/referral-program") {
            containerStyles = scrollBarStyleFullWidth;
        } else {
            containerStyles = scrollBarStyle;
        }

        return (
                <main className={getStyle("main")}>
                    <HeaderComponent/>
                    <Scrollbars onScroll={this.hideOverlaysOnScroll} autoHide={true} universal={true} style={containerStyles} renderTrackHorizontal={props => <div {...props} style={{display: "none"}}/>}>
                        <section className={initStyle(getStyle("content"), isPortfolio && getStyle("content", null, "width1500"))}>{}</section>
                    </Scrollbars>
                </main>
        );
    }
}

export default LayoutComponent;
