import {IconComponent} from "_component/IconComponent";
import {PureComponent} from "_component/PureComponent";
import {getStyle, initStyle} from "_utils/rBem";
import * as React from "react";
import {IButtonComponent} from "./interface";

export class ButtonComponent extends React.PureComponent<IButtonComponent, {}> {

    public static defaultProps: IButtonComponent = {
        iconPosition: "left",
        className: getStyle("button"),
        type: "button",
        title: "",
    };

    public render() {

        const {type, className, icon, iconPosition, iconClass, title, titleClass, ...otherProps} = this.props;
        const classes = initStyle(getStyle("button"), className);
        const iconRendered = <IconComponent name={icon} className={iconClass} />;

        return (
            <button className={classes} type={type} {...otherProps}>
                {!!icon && iconPosition === "left" && iconRendered}
                <PureComponent tag="span" className={titleClass}>{title}</PureComponent>
                {!!icon && iconPosition === "right" && iconRendered}
            </button>
        );
    }
}

export default ButtonComponent;
