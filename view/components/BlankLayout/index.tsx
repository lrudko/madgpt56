import {FooterSection, HeaderInfoBlock} from "_blocks";
import BackdropComponent from "_component/BackdropComponent";
import {getStyle} from "_utils/rBem";
import * as React from "react";

export class BlankLayout extends React.Component<{}, {}> {

    constructor(props){
        super(props);
    }

    public render() {
        return [
            <HeaderInfoBlock lite={true} key={"HeaderInfoBlock"}/>,
            (
                <section key={"contentBlock"} style={{minHeight: "70vh", paddingTop: "77px", background: "#EAEEF7"}} className={getStyle("content")}>{}</section>
            ),
            <BackdropComponent key={"BackdropComponent"} hideOnClick={true}/>,
            <FooterSection key={"FooterSection"}/>,
        ];
    }
}

export default BlankLayout;
