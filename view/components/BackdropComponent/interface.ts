import {HomeStoreClass} from "_store/home";
import {classNames} from "_style";

export interface IBackdropComponent {

    home?: HomeStoreClass;

    className?: classNames;
    hideOnClick?: boolean;
}
