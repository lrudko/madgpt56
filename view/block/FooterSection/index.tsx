import ImageComponent from "_component/ImageComponent";
import LinkComponent from "_component/LinkComponent";
import {IFooterNav, INavItem, NavigationClass} from "_store/navigation";
import {getStyle} from "_utils/rBem";
import {inject, observer} from "mobx-react";
import * as React from "react";

const ItemLink = ({title, href, ...props}: INavItem) => (<li className={getStyle("footer", "list-item")}><LinkComponent href={href} social={true} title={title} {...props}/></li>);

const FooterList = ({title, items}: IFooterNav) => (
    <div className={getStyle("footer", "site-map")}>
        <div className={getStyle("footer", "title")}>{title}</div>
        <ul className={getStyle("footer", "link-list")}>
            {items.map((props, key) => (<ItemLink key={"item" + key} {...props} />))}
        </ul>
    </div>
);

@inject("navigation")
@observer
export class FooterSection extends React.Component<{ navigation?: NavigationClass}, {}> {
    public state = {
        cookiePolicyHasRead: localStorage.getItem("CookiePolicyHasRead"),
    };

    constructor(props: any){
        super(props);
        this.setCookiePolicyHasRead = this.setCookiePolicyHasRead.bind(this);
    }
    public render() {
        const displayBorders = window.location.pathname === "/security";
        const {getFooter} = this.props.navigation;
        return (
            <div style={{position: "relative"}}>
                {displayBorders && <div className={getStyle("security-main", "bounding")}/>}
                <div className={getStyle("footer")}>
                    <div className={getStyle("footer", "block")}>
                        <div className={getStyle("footer", "row-logo")}>
                            <ImageComponent alt={"BitsGap"} name={"logo-white"} type={"png"}/>
                            <div className={getStyle("footer", "container", "contacts")}>
                                Bitsgap Holding OÜ is registered in Estonia<br />
                                Registry code 14004763,<br />
                                Address Mõisa str 4, 13522 Tallinn, Estonia
                            </div>

                        </div>
                        <div className={getStyle("footer", "container")}>
                            {getFooter.map((props, key) => <FooterList key={"footer" + key} {...props} />)}
                            <div className={getStyle("footer", "cm-logo")}>
                                <p>Marketing by </p>
                                <img alt={"Coinmedia"} src={require("_images/cm_logo.svg")}/>
                            </div>
                        </div>
                        <div className={getStyle("footer", "privacy")}>
                            © 2018 by Bitsgap
                        </div>
                    </div>
                    {window.location.pathname === "/"
                    && !this.state.cookiePolicyHasRead
                    && <div className={getStyle("footer", "cookie-privacy")}>
                        <p>We use cookies to provide you with a more convenient and safer user experience.
                                                 By continuing the browsing session or clicking the "OK" button, you confirm that you agree to the use of cookies.
                                                 You can revoke your consent at any time by changing your browser settings and deleting the cookies
                                                 stored. <a target="_blank" href="/cookie-privacy">Read our Cookie Policy</a>.
                            <button onClick={() => {this.setCookiePolicyHasRead()}}>OK</button>
                        </p>

                    </div>}
                </div>
            </div>
        );
    }

    private setCookiePolicyHasRead(){
        localStorage.setItem("CookiePolicyHasRead", "1");
        this.setState({cookiePolicyHasRead: true});
    }
}

export default FooterSection;
