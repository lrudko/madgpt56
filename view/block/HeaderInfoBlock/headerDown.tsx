import IconComponent from "_component/IconComponent";
import {getStyle} from "_utils/rBem";
import * as React from "react";

export class HeaderDown extends React.Component<{scrollTo: string, style?: any}> {

    public render() {
        return (
            <div
                style={this.props.style}
                className={getStyle("header-info", "down")}
                onClick={() => {document.getElementById(this.props.scrollTo).scrollIntoView({ behavior: "smooth", block: "start"}); }}
            >
                <IconComponent name={"ellipse"} className={getStyle("header-info", "ellipse-first")}/>
                <IconComponent name={"ellipse"} className={getStyle("header-info", "ellipse-second")}/>
                <IconComponent name={"chevron-up"} className={getStyle("header-info", "chevron")}/>
            </div>
        );
    }
}

export default HeaderDown;
