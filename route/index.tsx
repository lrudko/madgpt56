import {history} from "_store/routing";
import * as React from "react";
import {Router} from "react-router";
import ClientRoute from "./clientRoute";

let AppComponent;

if (process.env.NODE_ENV === "production") {
    AppComponent = (props: Router) => (<Router history={history} {...props}><ClientRoute /></Router>);
} else {
    const DevTools = require("mobx-react-devtools").default;
    AppComponent = (props: Router) => (<Router history={history} {...props}><div><ClientRoute /><DevTools /></div></Router>);
}
(window.location.search.substr(1)).split("&").forEach((row) => {
    const param = row.split("=");
    if (param[0] === "ref" || param[0] === "p"){
        localStorage.setItem("regURI", param[1]);
    }
});
export default AppComponent;
