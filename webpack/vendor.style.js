"use strict";

const resolve = require("path").resolve;
const vendor = [
    resolve(__dirname, "..", "styles", "base.css"),
    resolve(__dirname, "..", "styles", "base.scss"),
    resolve(__dirname, "..", "node_modules", "react-select/dist/react-select.min.css"),
    resolve(__dirname, "..", "node_modules", "react-virtualized/styles.css"),
    // resolve(__dirname, "..", "node_modules", "leaflet/dist/leaflet.css"),
    // resolve(__dirname, "..", "styles", "custom.css"),
];
exports.default = vendor;