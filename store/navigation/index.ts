import {action, computed, observable} from "mobx";

export interface INavItem {
    href: string;
    title: string;
    active?: boolean;
    clicked?: () => void;
    target?: any;
    icon?: any;
}

export interface IFooterNav {
    title: string;
    items: INavItem[];
}

interface INavList {
    list?: INavItem[];
    footer?: IFooterNav[];
    activeIndex?: number;
    showLanguages?: boolean;
}

export class NavigationClass implements INavList {

    @observable public showLanguages: boolean = false;

    @observable public activeIndex: number = 0;

    @observable public list = [
        {
            href: "/",
            title: "Home",
        },
        {
            title: "Terms of Service",
            href: "/terms-conditions",
        },
    ];

    @observable public footer  = [
        {
            title: "About",
            items: [
                {
                    title: "Company",
                    href: "#",
                },
            ],
        },
        {
            title: "Support",
            items: [
                {
                    title: "Help Center",
                    href: "http://helpdesk.bitsgap.com/support/home",
                    target: "_blank",
                    rel: "noopener",
                },
                {
                    title: "Terms of Service",
                    href: "/terms-conditions",
                },
            ],
        },
        {
            title: "Follow us",
            items: [
                {
                    title: "Bitcointalk",
                    href: "https://bitcointalk.org/index.php?topic=2864214",
                    target: "_blank",
                    icon: "bitcointalk",
                    rel: "noopener",
                },
                {
                    title: "Facebook",
                    href: "https://www.facebook.com/Bitsgap/",
                    target: "_blank",
                    icon: "facebook",
                    rel: "noopener",
                },
                {
                    title: "Twitter",
                    href: "https://twitter.com/Bitsgap/",
                    target: "_blank",
                    icon: "twitter",
                    rel: "noopener",
                },
                {
                    title: "Telegram",
                    href: "https://t.me/Bitsgap",
                    target: "_blank",
                    icon: "telegram",
                    rel: "noopener",
                },
            ],
        },
    ];

    constructor(props?: INavList) {
        if (typeof props !== "undefined") {
            const {list} = props;
            this.list = list;
        }
        this.setShowLanguages = this.setShowLanguages.bind(this);
    }

    @computed
    public get getList(): INavItem[] {
        return this.list;
    }

    @action
    public setList(list: INavItem[]): void {
        this.list = list;
    }

    @computed
    public get getFooter(): IFooterNav[] {
        return this.footer;
    }

    @action
    public setFooter(list: IFooterNav[]): void {
        this.footer = list;
    }

    @computed
    public get getActiveIndex(): number {
        return this.activeIndex;
    }

    @action
    public setActiveIndex(activeIndex: number): void {
        this.activeIndex = activeIndex;
    }

    @computed
    public get getShowLanguages(): boolean {
        return this.showLanguages;
    }

    @action
    public setShowLanguages(show: boolean): void {
        this.showLanguages = show;
    }

}

export const Navigation = new NavigationClass(process.env.BROWSER && (window as any).__initState__.navigation || undefined);
export default Navigation;
