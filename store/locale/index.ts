import {action, computed, observable} from "mobx";

export interface ILocaleItem {
    key: string;
}

export interface ILocaleStore {
    locales?: ILocaleItem[];
    locale: ILocaleItem;
}

export class LocaleStoreClass implements ILocaleStore {
    @observable public locales = [{key: "en"}/*, {key: "ru"}*/];
    @observable public locale = this.locales[0];

    constructor() {
        this.setLocale = this.setLocale.bind(this);
    }

    @action
    public setLocale(locale: ILocaleItem) {
        this.locale = locale;
    }

    @computed
    public get getLocale() {
        return this.locale;
    }

    @computed
    public get getLocales() {
        return this.locales;
    }
}

export const LocaleStore = new LocaleStoreClass();
export default LocaleStore;
