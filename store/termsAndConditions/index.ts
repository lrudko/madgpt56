import { action, computed, observable } from "mobx";

export interface ITermsAndConditions {
    hideTNC?: boolean;
}

export class TermsAndConditionsClass implements ITermsAndConditions {
    @observable public hideTNC: boolean = true;

    @computed public get getHidden(): boolean {
        return this.hideTNC;
    }

    @action public show() {
        document.body.style.overflow = "hidden";
        const html = document.getElementsByTagName("html")[0];
        (html as any).style.overflowY = "hidden";
        return this.hideTNC = false;
    }

    @action public hide() {
        document.body.style.overflowY = "initial";
        const html = document.getElementsByTagName("html")[0];
        (html as any).style.overflowY = "initial";

        return this.hideTNC = true;
    }

    @action public toggle() {
        this.hideTNC = !this.hideTNC;
    }
}

export const TermsAndConditionsStore = new TermsAndConditionsClass();
export default TermsAndConditionsStore;
