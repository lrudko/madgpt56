export const trimDemo = (value: string) => {
    return value ? value.toString().replace(".demo", "") : value;
};
