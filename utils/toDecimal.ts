export const toDecimal = (num: number | string): string => {
    if (/\d+\.?\d*e[\+\-]*\d+/i.test(String(num))) {
        const zero = "0";
        const parts = String(num).toLowerCase().split("e"); // split into coeff and exponent
        const e = Number(parts.pop()); // store the exponential part
        let l = Math.abs(e); // get the number of zeros
        const   sign = e / l;
        const coeffArray = parts[0].split(".");
        if (sign === -1) {
            num = zero + "." + new Array(l).join(zero) + coeffArray.join("");
        }
        else {
            const dec = coeffArray[1];
            if (dec) { l = l - dec.length; }
            num = coeffArray.join("") + new Array(l + 1).join(zero);
        }
    }

    return String(num);
};

export default toDecimal;
